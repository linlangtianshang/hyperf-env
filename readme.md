[TOC]

## 安装
```bash
git clone git@192.168.1.163:liaoshanjin/hyperf.git

cp .env.example .env

docker compose up -d

docker compose stop hyperf

# 配置 hyperf 项目的 .env

docker compose restart hyperfs
```